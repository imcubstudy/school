#include <cstdio>
#include <set>
using namespace std;
typedef  unsigned long long llu;

struct num
{
	llu n;
	int a;
	int b;
	int c;
	num(llu n, int a, int b, int c) : n(n), a(a), b(b), c(c) {}
	num() : n(0), a(0), b(0), c(0) {}
	bool operator<(num b) const
	{
		return (this->n == b.n) ? (this->a < b.a || this->b < b.b || this->c < b.c)
								: (this->n < b.n);
	}
};

int main()
{
	freopen("calc.in", "r", stdin);
	freopen("calc.out", "w", stdout);
	llu n;
	int a, b, c;
	scanf("%llu%i%i%i", &n, &a, &b, &c);
	set<num> tree;
	tree.insert(num(n, 0, 0, 0));
	for( int i = 0; i < a + b + c; i++ )
	{
		set<num> newtree;
		for( set<num>::iterator it = tree.begin(); it != tree.end(); it++ )
		{
			if( it->a < a )
				newtree.insert(num(it->n / 2, it->a + 1, it->b, it->c));
			if( it->b < b )
				newtree.insert(num((it->n + 1) / 2, it->a, it->b + 1, it->c));
			if(it->c < c )
				newtree.insert(num(it->n > 0 ? (it->n - 1) / 2 : 0, it->a, it->b, it->c + 1));
		}
		tree = newtree;
	}
	printf("%llu\n", tree.begin()->n);
    return 0;
}
