#include <cstdio>
#include <algorithm>

typedef long long lli;

lli bin_search(lli n, lli a, lli b, lli w, lli h);

int main()
{
	freopen("space.in", "r", stdin);
	freopen("space.out", "w", stdout);
	lli n, a, b, w, h;
	scanf("%lli%lli%lli%lli%lli", &n, &a, &b, &w, &h);
	printf("%lli\n", std::max(bin_search(n, a, b, w, h), 
				  bin_search(n, b, a, w, h)));
	return 0;
}

lli bin_search(lli n, lli a, lli b, lli w, lli h)
{
	lli L = 0, R = std::min(w, h);
	while( R - L >  1 )
	{
		lli M = (L + R) / 2;
		lli kw = w / (a + 2 * M), kh = h / (b + 2 * M);
		if( kw * kh < n ) R = M;
		else L = M;
	}
	return L;
}
