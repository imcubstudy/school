#include <cstdio>

int main()
{
	freopen("prizes.in", "r", stdin);
	freopen("prizes.out", "w", stdout);
	long long N;
	scanf("%lli", &N);
	long long max = 0, underMax = 0;
	scanf("%lli", &max);
	for( int i = 0; i < N - 1; i++ )
	{
		long long tmp;
		scanf("%lli", &tmp);
		if( tmp >= max ) underMax = max, max = tmp;
		else if( tmp > underMax ) underMax = tmp;
		printf("%lli ", underMax);
	}
	printf("\n");
    	return 0;
}
