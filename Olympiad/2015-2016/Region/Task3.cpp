#include <cstdio>
#include <cstring>
#include <vector>
#include <set>
#include <algorithm>
using namespace std;

typedef long long ll;

const ll M = 'z' - 'a' + 1;

struct MyChar
{
	char value;
	ll counter;
	MyChar() : value(0), counter(0) {}
	MyChar(char value, ll counter) : value(value), counter(counter) {}
};

int main()
{
	freopen("strange.in", "r", stdin);
	freopen("strange.out", "w", stdout);

#pragma region Input

	char s[(unsigned)2e5 + 3];
	scanf("%s", s);
	vector<MyChar> chars((unsigned)2e5 + 3);
	chars[0] = MyChar(s[0], 1); ll size = 1;
	ll maximums[M] = { 0 };
	ll len = strlen(s);
	for( ll i = 1; i < len; i++ )
		if( s[i] == chars[size - 1].value ) chars[size - 1].counter++;
		else 
		{
			maximums[chars[size - 1].value - 'a'] = max(maximums[chars[size - 1].value - 'a'], chars[size - 1].counter);
			chars[size++] = MyChar(s[i], 1);
		}
	maximums[chars[size - 1].value - 'a'] = max(maximums[chars[size - 1].value - 'a'], chars[size - 1].counter);
	maximums[chars[	   0   ].value - 'a'] = max(maximums[chars[    0   ].value - 'a'], chars[    0   ].counter);

#pragma endregion

	ll ans = 0;

#pragma region SearchingForAnswer

	for( ll i = 0; i < M; i++ )
		ans += maximums[i];

	vector< vector< set< pair<ll, ll> > > > pairs(M, vector< set< pair<ll, ll> > >(M));
	for( ll i = 1; i < size; i++ )
	{
		pairs[chars[i - 1].value - 'a'][chars[i].value - 'a'].insert(
			make_pair(chars[i - 1].counter, chars[i].counter));
	}

	for( auto v : pairs )
		for( auto e : v )
		{
			if( e.empty() ) continue;
			auto end = e.end(); --end;
			ans += end->first * end->second;
			ll maxSecond = end->second;
			if( end != e.begin() )
			{
				--end;
				for( auto it = end; it != e.begin(); it-- )
				{
					ans += (it->first)*max((ll)0, (it->second) - maxSecond);
					maxSecond = max(maxSecond, it->second);
				}
				ans += e.begin()->first * max((ll)0, e.begin()->second - maxSecond);
			}
		}

#pragma endregion

	printf("%lli\n", ans);

	return 0;
}
