#include <cstdio>

int main()
{
	freopen("division.in", "r", stdin);
	freopen("division.out", "w", stdout);
	long long N;
	scanf("%lli", &N);
	switch( N % 3 )
	{
	case 0: { printf("%lli %lli %lli\n", N / 3 - 1, N / 3, N / 3 + 1);	break; }
	case 1: { printf("%lli %lli %lli\n", N / 3 - 1, N / 3, N / 3 + 2); 	break; }
	case 2: { printf("%lli %lli %lli\n", N / 3 - 1, N / 3 + 1, N / 3 + 2); 	break; }
	}
    	return 0;
}
