#include <vector>
#include <cstdio>
#include <algorithm>
using namespace std;

typedef long long ll;

struct Card
{
	ll value;
	ll count;
	ll valueAtK;
	Card() : value(0), count(0), valueAtK(0) {}
	Card(ll value, ll count, ll valueAtK) : value(value), count(count), valueAtK(valueAtK) {}
};

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	
#pragma region Input
	
	ll N, K;
	scanf("%lli%lli", &N, &K);

	vector<ll> values(N);
	for( ll i = 0; i < N; i++ )
		scanf("%lli", &values[i]);

	sort(values.begin(), values.end());
	vector<Card> cards(N); ll differentCounter = 1; cards[0] = Card(values[0], 1, values[0] * K);
	for( ll i = 1; i < N; i++ )
	{
		if( values[i] == cards[differentCounter - 1].value ) cards[differentCounter - 1].count++;
		else cards[differentCounter++] = Card(values[i], 1, values[i] * K);
	}
	
#pragma endregion
	
	ll ans = 0;
	
#pragma region SearchingforAnswer

	ll breaker = 0, skippedWithMoreThanTwo = (cards[0].count >= 2) ? 1 : 0;
	for( ll i = 0; i < differentCounter; i++ )
	{
		while( cards[breaker + 1].value <= cards[i].valueAtK && breaker + 1 < differentCounter )
		{
			breaker++;
			if( cards[breaker].count >= 2 ) skippedWithMoreThanTwo++;
		}
		if( cards[i].count >= 3 ) ans++;
		if( cards[i].count >= 2 ) ans += (breaker - i)*3, skippedWithMoreThanTwo--;
		if( skippedWithMoreThanTwo > 0 ) ans += skippedWithMoreThanTwo*3;
		ans += (breaker - i)*(breaker - i - 1)*3;
	}

#pragma endregion

	printf("%lli\n", ans);
	return 0;
}
