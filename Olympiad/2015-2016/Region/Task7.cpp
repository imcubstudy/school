#include <cstdio>
#include <cstring>

typedef unsigned long long ull;

const ull MOD = 1000000007;
ull table[10][101];
char L[101], R[101];

void build_table();
ull get_count(char* s);
ull is_interest(char* s);

int main()
{
	freopen("numbers.in", "r", stdin);
	freopen("numbers.out", "w", stdout);
	
	scanf("%s%s", L, R);
	build_table();

	long long ans = get_count(R) - get_count(L) + is_interest(R);
	if( ans >= 0 ) printf("%lli\n", ans);
	else printf("%lli\n", MOD + ans);

    	return 0;
}

void build_table()
{
	ull nMax = strlen(R);
	for( ull i = 1; i < 10; i++ )
		table[i][1] = 1;
	for( ull i = 1; i <= nMax; i++ )
		table[9][i] = 1;
	for( ull n = 2; n <= nMax; n++ )
		for( ull i = 8; i > 0; i-- )
			table[i][n] = (table[i + 1][n] + table[i][n - 1]) % MOD;
	for( ull n = 1; n <= nMax; n++ )
	{
		ull sum = 0;
		for( ull i = 9; i > 0; i-- ) sum = (sum + table[i][n]) % MOD;
		table[0][n] = sum;
	}
}

ull get_count(char* s)
{
	ull res = 0, nMax = strlen(s);
	for( ull i = 1; i < s[0] - '0'; i++ )
		res = (res + table[i][nMax]) % MOD;
	for( ull n = 1; n < nMax; n++ )
	{
		if( s[n] >= s[n - 1] )
		{
			for( ull i = s[n-1] - '0'; i < s[n] - '0'; i++ )
				res = (res + table[i][nMax - n]) % MOD;
		} else break;
	}
	for( int n = 1; n < nMax; n++ )
		res = (res + table[0][n]) % MOD;
	return res;
}

ull is_interest(char* s)
{
	for( ull i = 0; i < strlen(s) - 1; i++ )
		if( s[i] > s[i + 1] ) return 0;
	return 1;
}
