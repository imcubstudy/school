#include <cstdio>
#include <set>
using namespace std;

typedef long long ll;

int main()
{
	freopen("d.in", "r", stdin);
	freopen("d.out", "w", stdout);
	ll N;
	scanf("%lli", &N);
	set<ll> nums;
	for( ll i = 0; i < N; i++ )
	{
		ll tmp;
		scanf("%lli", &tmp);
		if( nums.find(tmp) == nums.end() )
		{
			nums.insert(tmp);
			printf("%lli\n", tmp);
		}
	}
    return 0;
}

