#include <iostream>
#include <vector>
using namespace std;

int main()
{
    int n, m;
    cin >> n >> m;
    vector <int> A(n);
    for (int i = 0; i < n; ++i)
        cin >> A[i];
    vector <bool> F(m + 1, 0);
    F[0] = true;
    for (int i = 0; i < n; ++i)
    {
        for (int k = m; k >= A[i]; --k)
            if (F[k - A[i]])
                F[k] = true;
    }
    while (F[m] == false) --m;
    cout << m << endl;
    return 0;
}