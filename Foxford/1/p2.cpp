#include<iostream>
#include<vector>

using namespace std;

int main()
{
    int n, i, j;
    cin >> n;
    vector <bool> is_prime(n + 1, true);
    int ans = 0;
    for (int i = 2; i <= n; ++i)
        if (is_prime[i])
        {
            ans++;
            for (int j = i * i; j <= n; j += i)
                is_prime[j] = false;
        }
    cout << ans << endl;
    return 0;
}