#include <iostream>
#include <vector>
using namespace std;

int main()
{
    int n;
    cin >> n;
    vector <int> F(31, 0);
    F[1] = 1;
    F[2] = 1;
    F[3] = 2;
    for (int i = 4; i <= n; ++i)
        F[i] = F[i - 1] + F[i - 2] + F[i - 3];
    cout << F[n] << endl;
    return 0;
}