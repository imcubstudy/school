#include <iostream>
#include <cmath>
using namespace std;

int k;

double f(double x)
{
    return cos(x) - k * x;
}

int main()
{
    cin >> k;
    double left = 0, right = 1.57;
    while (right - left >= 1e-15)
    {
        double middle = (left + right) / 2;
        if (f(middle) > 0)
            left = middle;
        else
            right = middle;
    }
    cout.precision(16);
    cout << (left + right) / 2 << endl;
    return 0;
}