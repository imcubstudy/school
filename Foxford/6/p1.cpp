#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main()
{
    int n, k, i, x;
    cin >> n >> k;
    vector<int> a(n);
    for (i = 0; i < n; ++i)
        cin >> a[i];
    for (i = 0; i < k; ++i)
    {
        cin >> x;
        if (binary_search(a.begin(), a.end(), x))
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
    return 0;
}