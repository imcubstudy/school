#include <iostream>
using namespace std;

int main()
{
    int N, k;
    cin >> N >> k;
    k--;
    while (k >= 0)
    {
        cout << (N >> k & 1);
        k--;
    }
    cout << endl;
    return 0;
}