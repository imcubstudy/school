#include <iostream>
using namespace std;

int main()
{
    int N, k;
    cin >> N >> k;
    cout << (N & ~(1 << k)) << endl;
    return 0;
}