#include <iostream>
using namespace std;

int main()
{
    int N, i = 0;
    cin >> N;
    while ((1 << i) <= N)
        i++;
    i--;
    while (i >= 0)
    {
        cout << (N >> i & 1);
        i--;
    }
    cout << endl;
    return 0;
}