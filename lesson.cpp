#include <iostream>
#include <string>
#include <algorithm>
#include <math.h>
using namespace std;

int main()
{
	cout << "Input p: ";
	int P; cin >> P;
	cout << "Input num: ";
	string num; cin >> num;
	bool comma = false;
	long long correction = 0;
	auto it = find(num.begin(), num.end(), '.');
	if( it != num.end() )
	{
		comma = true; correction = pow(P, (num.end() - it - 1));
		num.erase(it);
	}
	int* arr = new int[num.size()]{0};
	long long ans = 0;
	for( int i = 0; i < num.size(); i++ )
		arr[i] = (num[i] >= '0' && num[i] <= '9') ? num[i] - '0' : num[i] - 'A' + 10;
	for( int i = 0; i < num.size(); i++ )
	{
		ans = ans*P + arr[i];
		if( arr[i] >= P ) {	ans = 0; break;	}
	}
	if( !comma )cout << ans << endl;
	else cout << (double)ans / correction << endl;
    return 0;
}
