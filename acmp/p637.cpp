#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt","r");
    FILE* fout = fopen("OUTPUT.txt","w");
    int n;
    fscanf(fin, "%d", &n);
    int* arr = new int[n];
    for(int i = 0; i < n; i++)
        fscanf(fin, "%d", arr + i);
    int m;
    fscanf(fin, "%d", &m); 
    long long int ans = 0;
    for(int i = 0; i < n; i++)
        if(arr[i] < m) ans += arr[i];
    else ans += m;
    fprintf(fout, "%lli", ans);
    return 0;
}