#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    int a,b;
    fscanf(fin, "%d%d", &a, &b);
    fclose(fin);
    if(a > b) { int tmp = a; a = b; b = tmp;}
    int ans = a;
    while(ans % b != 0)
        ans += a;
    FILE* fout = fopen("OUTPUT.txt", "w");
    fprintf(fout, "%d", ans);
    fclose(fout);
    return 0;
}