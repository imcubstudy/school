def factorial(n):
    res = 1
    for i in range(1, n + 1):
        res *= i
    return res

fin = open("INPUT.txt", 'r')
fout = open("OUTPUT.txt", 'w')
ans = 0
N = int(fin.read())
for i in range(1 , N + 1):
    ans += factorial(i)
fout.write(str(ans))
fout.close()
