#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int a, b, c;
    fscanf(fin, "%d%d%d", &a, &b, &c);
    if(a*b == c)
    	fprintf(fout, "YES");
    else
      	fprintf(fout, "NO");
    return 0;
}