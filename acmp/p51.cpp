#include <cstdio>

int main()
{
    int n;
    FILE* fin = fopen("INPUT.txt", "r");
    fscanf(fin, "%d", &n);
    int k = 0;
    char dump = ' ';
    while(fscanf(fin, "%c", &dump) == 1)
        if(dump == '!') k++;
    fclose(fin);
    int ans = 1;
    while(n > 0)
    {
        ans *= n;
        n -= k;
    }
    FILE* fout = fopen("OUTPUT.txt", "w");
    fprintf(fout, "%d", ans);
    fclose(fout);
    return 0;
}