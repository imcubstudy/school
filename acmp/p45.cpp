#include <cstdio>
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    unsigned long num;
    fscanf(fin,"%lu", &num);
    unsigned long long ans = 0;
    unsigned long long i = 0;
    bool output = false;
    if(num >= 2)
    {
        while(num != 1)
        {
            long long ch;
            bool found=false;
            for(ch = 9; ch > 1; ch--)
                if(num % ch == 0) { found = true; break;}
            if(found)
            {
                 num /= ch;
                 for(int j = 0; j < i; j++)
                     ch*= 10;
                 i++;
                 ans += ch;
            }
            else
            {
                 fprintf(fout, "-1");
                 output = true;
                 break;
            }
         }
    }else if(num == 0){ fprintf(fout, "10"); output = true;}
    else { fprintf(fout, "1"); output = true;}
    if(!output) fprintf(fout, "%lli", ans);
    return 0;
}