#include <cstdio>
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    char s[] = "qwertyuiopasdfghjklzxcvbnmq";
    char ch;
    fscanf(fin, "%c", &ch);
    for(int i = 0; true; i++)
        if(s[i] == ch) { fprintf(fout, "%c", s[i+1]); break; }
    return 0;
}