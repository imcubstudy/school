#include <cstdio>
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    unsigned max = 0, min = -1, tmp;
    while(fscanf(fin, "%d", &tmp) == 1)
    {
        if(tmp > max) max = tmp;
        if(tmp < min) min = tmp;
    }
    fclose(fin);
    FILE* fout = fopen("OUTPUT.txt", "w");
    fprintf(fout, "%d", max - min);
    fclose(fout);
    return 0;
}