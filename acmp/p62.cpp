#include <cstdio>
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    char ch; int si;
    fscanf(fin, "%c%d", &ch, &si);
    bool white = false;
    if((ch - 'A' + 1) % 2 == 0)
    {
        if(si % 2 == 1) white = true; 
    }
    else if(si % 2 == 0) white = true;
    if(white) fprintf(fout, "WHITE");
    else fprintf(fout, "BLACK");
    return 0;
}