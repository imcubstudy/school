#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int a, b, n;
    fscanf(fin, "%d%d%d", &n, &a, &b);
    fprintf(fout, "%d", 2*a*b*n);
    return 0;
}
