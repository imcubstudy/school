def factorial(n):
    res = 1
    for i in range(1, n + 1):
        res *= i
    return res

f = open("INPUT.txt", 'r')
n = int(f.read())
f.close()
fout = open("OUTPUT.txt", 'w')
fout.write(str(factorial(n)))
fout.close()
