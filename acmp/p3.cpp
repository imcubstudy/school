#include <fstream>
using namespace std;

int main()
{
	ifstream infile("input.txt");
	ofstream outfile("output.txt");
	long long int n;
	infile >> n;
	n *= n;
	outfile << n;
	return 0;
}