#include <cstdio>
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int n, m;
    fscanf(fin, "%d%d", &n, &m);
    int* arr = new int[n];
    for(int k = 0; k < n; k++)
        arr[k] = 0;
    for(int k = 0; k < m; k++)
    {
        int i,j;
        fscanf(fin, "%d%d", &i, &j);
        arr[i-1]++;
        arr[j-1]++;
    }
    for(int k = 0; k < n; k++)
        fprintf(fout, "%d ", arr[k]);
    return 0;
}