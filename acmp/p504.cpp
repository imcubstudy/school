#include <cstdio>

inline void swap(char &a, char &b)
{
    char tmp = a;
    a = b;
    b = tmp;
}

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    char s[] = "GCV";
    int n;
    fscanf(fin, "%d", &n);
    for(int i = 0; i < n; i++)
    {
        swap(s[2], s[1]);
        swap(s[0], s[1]);
    }
    fprintf(fout, "%s", s);
    return 0;
}