#include <cstdio>

int main()
{
	FILE* fin = fopen("INPUT.txt", "r");
	FILE* fout = fopen("OUTPUT.txt", "w");
	long long int a, b;
	fscanf(fin, "%lli%lli", &a, &b);
	fprintf(fout, "%lli", a + b);
	return 0;
}