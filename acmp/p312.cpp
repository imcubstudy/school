#include <cstdio>
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int a1, a2, n;
    fscanf(fin, "%d%d%d", &a1, &a2, &n);
    int d = a2-a1;
    fprintf(fout, "%li", a1 + (long)d*(n - 1)); 
    return 0;
}