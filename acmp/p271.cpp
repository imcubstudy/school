#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    unsigned long int N;
    fscanf(fin, "%lu", &N);
    int count = 3;
    unsigned long int fib[3];
    fib[0] = 1;
    fib[1] = 1;
    fib[2] = 2;
    while(fib[2] < N)
    {
        fib[0] = fib[1];
        fib[1] = fib[2];
        fib[2] = fib[1] + fib[0];
        count++;
    }
    if(fib[2] == N)
        fprintf(fout, "1\n%d", count);
    else
        fprintf(fout, "0");
    fclose(fin);
    fclose(fout);
    return 0;
}