#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    int n;
    fscanf(fin, "%d", &n);
    int** arr = new int*[n];
    for(int i = 0; i < n; i++)
        arr[i] = new int[n];
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            fscanf(fin, "%d", &arr[i][j]);
    fclose(fin);
    int ans = 0;
    for(int i = 0; i < n; i++)
        for(int j = i + 1; j < n; j++)
            if(arr[i][j]) ans++;
    FILE* fout = fopen("OUTPUT.txt", "w");
    fprintf(fout, "%d", ans);
    fclose(fout);
    return 0;
}