#include <cstdlib>
#include <cstdio>
using namespace std;

int compare(const void* a, const void* b)
{
    return *(int*)a - *(int*)b;
}

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int n;
    fscanf(fin, "%d", &n);
    int* arr = new int[n];
    for(int i = 0; i < n; i++)
        fscanf(fin, "%d", arr + i);
    qsort(arr, n, sizeof(int), compare);
    for(int i = 0; i < n; i++)
        fprintf(fout, "%d ", arr[i]);
    return 0;
}