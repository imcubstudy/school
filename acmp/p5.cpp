#include <fstream>
using namespace std;
 
int main()
{
  ifstream infile("INPUT.txt");
  ofstream outfile("OUTPUT.txt");
  int threes[100], fours[100], n_threes = 0, n_fours = 0, n_days;
  infile >> n_days;
  for(int i = 0; i < n_days; i++)
  {
    int tmp;
    infile >> tmp;
    if(tmp%2 == 0) fours[n_fours++] = tmp;
    else threes[n_threes++] = tmp;
  }
  for( int i = 0; i < n_threes; i++)
    outfile << threes[i] << " ";
  outfile << endl;
  for ( int i = 0; i < n_fours; i++)
    outfile << fours[i] << " ";
  if(n_fours >= n_threes)
    outfile << "\nYES";
  else
    outfile << "\nNO";
  return 0;  
}