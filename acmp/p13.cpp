#include <fstream>
using namespace std;

int main()
{
	ifstream infile("INPUT.txt");
	ofstream outfile("OUTPUT.txt");
	int ch, dog;
	infile >> ch >> dog;
	int c1, c2, c3, c4;
	c1 = ch % 10;
	c2 = ch % 100 / 10;
	c3 = ch % 1000 / 100;
	c4 = ch % 10000 / 1000;
	int d1, d2, d3, d4;
	d1 = dog % 10;
	d2 = dog % 100 / 10;
	d3 = dog % 1000 / 100;
	d4 = dog % 10000 / 1000;
	int b = 0;
	if (c1 == d1) b++;
	if (c2 == d2) b++;
	if (c3 == d3) b++;
	if (c4 == d4) b++;
	int k = 0;
	if (c1 == d2 || c1 == d3 || c1 == d4) k++;
	if (c2 == d1 || c2 == d3 || c2 == d4) k++;
	if (c3 == d1 || c3 == d2 || c3 == d4) k++;
	if (c4 == d1 || c4 == d2 || c4 == d3) k++;
	outfile << b << " " << k;
	return 0;
}