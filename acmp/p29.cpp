#include <cstdio>

inline long long abs(long long a)
{
    if(a < 0) a *= -1;
    return a;
}
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int n;
    fscanf(fin, "%d", &n);
    if(n >= 2)
    {
        int heights[3];
        fscanf(fin, "%d", heights);
        fscanf(fin, "%d", heights + 1);
        long long unsigned int ans[3] = {0};
        ans[1] = abs(heights[1] - heights[0]);
        int i;
        for(i = 2; i <= n; i++)
        {
            fscanf(fin,"%lli", heights + 2);
            long long unsigned normal = abs(heights[2] - heights[1]);
            long long unsigned boost = 3*abs(heights[2] - heights[0]);
            if(normal + ans[1] > boost + ans[0]) ans[2] = ans[0] + boost;
            else ans[2] = normal + ans[1];
            ans[0] = ans[1];
            ans[1] = ans[2];
            heights[0] = heights[1];
            heights[1] = heights[2];
        }
        fprintf(fout, "%llu", *ans);
    }else fprintf(fout, "0");
    fclose(fin);
    fclose(fout);
    return 0;
}