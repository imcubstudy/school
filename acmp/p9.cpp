#include <cstdio>
   
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    int n;
    fscanf(fin, "%d", &n);
    int* arr = new int[n];
    for(int i = 0; i < n; i++)
        fscanf(fin, "%d", arr + i);
    fclose(fin);
    int pos_sum = 0, indmin = 0, indmax = 0;
    for(int i = 0; i < n; i++)
    {
        if(arr[i] > 0) pos_sum += arr[i];
        if(arr[i] < arr[indmin]) indmin = i;
        else if(arr[i] > arr[indmax]) indmax = i;
    }
    if(indmin > indmax) { int tmp = indmin; indmin = indmax; indmax = tmp;}
    int betw_mult = 0;
    if(indmax - indmin > 1)
    {
        betw_mult = 1;
        for(int i = indmin + 1; i < indmax; i++)
            betw_mult *= arr[i];
    }
    delete[] arr;
    FILE* fout = fopen("OUTPUT.txt", "w");
    fprintf(fout, "%d %d", pos_sum, betw_mult);
    fclose(fout);
    return 0;
}