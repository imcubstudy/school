#include <fstream>
#include <string>
using namespace std;

string max(string a, string b)
{
    int a_len = a.size(),
        b_len = b.size();
    if(a_len > b_len) 
        return a;
    else if(a_len == b_len) 
        if(a > b) return a;
    return b;
}

int main()
{
    ifstream infile("INPUT.txt");
    string a, b, c;
    infile >> a >> b >> c;
    ofstream outfile("OUTPUT.txt");
    outfile << max( max(a, b), c);
    return 0;
}