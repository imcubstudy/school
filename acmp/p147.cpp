#include <cstdio>
 
int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int n;
    fscanf(fin, "%d", &n);
    int fib[3] = {0,1,1};
    for(int i = 0; i < n; i++)
    {
        fib[2] = fib[0] + fib[1];
        fib[0] = fib[1];
        fib[1] = fib[2];
    }
    fprintf(fout, "%d", fib[0]);
    return 0;
}