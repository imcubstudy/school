#include <cstdio>

int main()
{
    FILE* fin = fopen("INPUT.txt", "r");
    FILE* fout = fopen("OUTPUT.txt", "w");
    int n;
    fscanf(fin, "%d", &n);
    long int ans = n + 1;
    for(int i = 2; i < n; i++)
    if(n%i == 0) ans += i;
    fprintf(fout, "%li", ans);
    return 0;
}