fin = open("INPUT.txt", 'r')
lstinp = fin.read().split(' ')
K = int(lstinp[0])
N = int(lstinp[1])
fin.close()

ans = [0] * (N + 1)

for i in range(1, N + 1):
    if i <= K:
        ans[i] = 1
    for j in range(i - K, i):
        if j > 0:
            ans[i] += ans[j]

fout = open("OUTPUT.txt", 'w')
fout.write(str(ans[N]))
fout.close()
