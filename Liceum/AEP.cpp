#include <iostream>
#include <string>
using namespace std;

string getstring(int n)
{
	string s;
	int tmp = n % 2, mask = 1;
	n = n >> 1;
	while( n > 1 )
	{
		if( tmp == 1 && n&mask == 1 )
			s += 'A', tmp = n >> 1 & mask, n = n >> 2;
		else if( tmp == 1 )
			s += 'P', tmp = n >> 1 & mask, n = n >> 2;
		else
			s += 'E', tmp = n & mask, n = n >> 1;
	} 
	if( s == "" ) return "E";
	else return s;
}

int main()
{
	int num;
	cin >> num;
	string s = getstring(num);
	int ans = 0;
	for( auto e : s )
		if( e == 'A' || e == 'E') ans++;
	cout << s << endl << ans << endl;
    return 0;
}

