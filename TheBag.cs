﻿using System;

namespace TheBag
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите вместимость рюкзака: "); int max = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите количесво вещей: ");     int N = Convert.ToInt32(Console.ReadLine());
           
            Console.Write("Введите вес вещей: ");
            int maxSum = 0;
            string[] input = Console.ReadLine().Split(' ');
            int[] thingsWeight = new int[N];
            for(int i = 0; i < N; i++)
            {
                thingsWeight[i] = Convert.ToInt32(input[i]);
                maxSum += thingsWeight[i];
            }

            bool[] possibleWeight = new bool[maxSum + 1];
            for (int i = 0; i < maxSum + 1; i++ )
                possibleWeight[i] = false;
            possibleWeight[0] = true;

            foreach(int weight in thingsWeight)
                for (int j = maxSum; j >= 0; j--)
                    if (possibleWeight[j]) possibleWeight[j + weight] = true;

            for (int i = max; i >= 0; i--)
                if (possibleWeight[i])
                {
                    Console.WriteLine("Максимальная загрузка " + i.ToString() + " кг.");
                    break;
                }
        }
    }
}
